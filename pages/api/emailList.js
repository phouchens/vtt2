const axios = require('axios');

export default async function (req, res) {
  const { email, name } = req.body;

  const options = {
    headers: { 'X-Auth-Token': `api-key ${process.env.GET_RESPONSE_API_KEY}` }
  };

  const data = {
    name,
    email,
    campaign: { campaignId: process.env.GET_RESPONSE_CAMPAIGN_ID }
  };

  try {
    const URL = 'https://api.getresponse.com/v3/contacts';
    const response = await axios.post(URL, data, options);
    console.log(response.status);
    res.status(200).send('you are now signed up');
  } catch (error) {
    console.log(error);
    res.status(500).send(error.message);
  }
}

import PodcastsHeaderContainer from '@components/halfScreenContainers/PodcastsHeaderContainer';
import PodcastContainer from '@components/podcastsPageLayout/PodcastContainer';
import { vttOrange } from '@styles/theme';
import { Box, Heading, Text } from 'grommet';
import { LINKS } from 'helpers/globals';
import Head from 'next/head';

const vttAbout =
  'Veteran Trash Talk Hour podcasts are stories of real warriors dealing with demons on the inside. We are brave enough to share them with our brothers, sisters, supporters, and contributors so we can defeat them together. Our podcasts promote veteran-owned businesses and organizations that support veteran causes. Also, there is plenty of trash talk.';

const fmwAbout =
  'Just when you thought the fellas were having all the fun, meet Leitha, Len, and Leah who have had their fair share of days when they just wanted to give someone a good ole throat punch. We will talk about the good, bad, and the ugly when it comes to service, life, all it entails, tackling specific issues that women veterans face.';

const stonedAbout =
  "The goal of THE STONED VET is to build the community around the VET!! Let's build the camaraderie that we lost when we left the military!!!! SFMF!!!!' This marine vet tackles today's issues and current events with a laid back sense of humor and relaxed vibe.";

const btbAbout = 'A hard charging marine talks life, lifting, and how to hustle.';

export default function Podcasts() {
  return (
    <>
      <Head>
        <title>Podcasts</title>
      </Head>
      <Box fill="horizontal">
        <PodcastsHeaderContainer />

        <Box fill="horizontal" align="center">
          <Heading level="2" color={vttOrange} margin={{ bottom: 'none' }}>
            Our Current Line Up
          </Heading>
          <Text margin="medium" textAlign="center">
            We take great pride in any Podcast that carries the VTT Brand. Each podcast is unique
            but share the common goals of building the veteran community, providing resources for
            soldiers and veterans, and promoting veteran businesses.
          </Text>
          <PodcastContainer
            src="/vtth.webp"
            title="THE VETERAN TRASH TALK HOUR"
            about={vttAbout}
            href={LINKS.YOUTUBE}
          />
          <PodcastContainer
            src="/tpm.webp"
            title="THROAT PUNCH MONDAY WITH LEAH, LEN AND LEITHA"
            about={fmwAbout}
            href={LINKS.TPM}
          />
          <PodcastContainer
            src="/stonedVet.webp"
            title="THE STONED VET"
            about={stonedAbout}
            href={LINKS.STONED_VET_YOUTUBE}
          />
          <PodcastContainer
            src="/BehindTheBeard.webp"
            title="BEHIND THE BEARD"
            about={btbAbout}
            href={LINKS.BTB}
          />
        </Box>
      </Box>
    </>
  );
}

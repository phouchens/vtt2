import React from 'react';
import { Box, ResponsiveContext } from 'grommet';
import Head from 'next/head';
import CallToAction from '../components/callToAction/CallToAction';
import ConnectBar from '../components/connectBar/ConnectBar';
import HeaderOneContainer from '../components/halfScreenContainers/HeaderOneContainer';
import SponsorContainer from '@components/halfScreenContainers/SponsorContainer';
import { LINKS } from '../helpers/globals';
import VttHourLayout from '../components/homePageLayout/VttHourLayout/VttHourLayout';
import MeetTheHosts from '../components/homePageLayout/MeetTheHosts/MeetTheHosts';
import Community from '../components/shared/Community';
import WhatPeopleAreSaying from '@components/whatPeopleAreSaying/WhatPeopleAreSaying';
import PARTNERS from 'helpers/partnerValues';
import BannerContainer from '@components/bannerContainer/BannerContainer';

export default function Home() {
  const size = React.useContext(ResponsiveContext);

  return (
    <>
      <Head>
        <title>Veteran Trash Talk</title>
      </Head>
      <Box fill="horizontal">
        <HeaderOneContainer />
        <Box direction="column" justify="center" margin={{ top: '1em' }}>
          <CallToAction size={size} />
          <BannerContainer src="/ZFAd.webp" href={LINKS.ZACH} color="#0f153f" />
          <VttHourLayout />
          <MeetTheHosts />
          <ConnectBar />
          <Community />
          <WhatPeopleAreSaying />
          <SponsorContainer
            featured
            title="Zach Farkas"
            info={PARTNERS.ZACH}
            src="/veteransMortgage.webp"
            href={LINKS.ZACH}
            width={500}
            height={100}
            showLightBg
            lightBgPad
          />
          <SponsorContainer
            featured
            title="Featured Sponsor: Ventura Training and Athletics"
            info="We offer a unique approach to strength training in combination with recovery and restoration techniques. We provide personalized, one-on-one service at a private and intimate studio the Ventura California. Our approach is straightforward. Our clients achieve their goals by tackling challenging, tailor-made workouts that are results-oriented, do not require an unrealistic time commitment, and are dependably safe"
            src="/VTA.webp"
            href={LINKS.VTA}
          />
        </Box>
      </Box>
    </>
  );
}

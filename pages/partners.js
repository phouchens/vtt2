import React from 'react';
import Head from 'next/head';
import { Box } from 'grommet';
import PartnerHeaderContainer from '@components/halfScreenContainers/PartnerHeaderContainer';
import SponsorContainer from '@components/halfScreenContainers/SponsorContainer';
import { LINKS } from 'helpers/globals';
import PARTNERS from 'helpers/partnerValues';

const Partners = () => {
  return (
    <>
      <Head>
        <title>Partners: Veteran Trash Talk</title>
      </Head>
      <Box fill="horizontal">
        <PartnerHeaderContainer />
        <Box pad="small">
          <SponsorContainer
            title="Stop Soldier Suicide"
            info={PARTNERS.SSS}
            src="/bigSSS.webp"
            href={LINKS.SSS}
            width={520}
            height={73}
          />
          <SponsorContainer
            title="Zachary Farkas"
            info={PARTNERS.ZACH}
            src="/veteransMortgage.webp"
            href={LINKS.ZACH}
            width={500}
            height={100}
            showLightBg
            lightBgPad
          />
          <SponsorContainer
            width={525}
            height={85}
            title="Neuro Flow"
            info={PARTNERS.NF_BLURB}
            src="/NFlow.webp"
            href={LINKS.NUERO_FLOW}
          />
          <SponsorContainer
            title="Ventura Training and Athletics"
            info={PARTNERS.VTA}
            src="/VTA.webp"
            href={LINKS.VTA}
          />
          <SponsorContainer
            title="10th Mountain Whisky & Spirit Company"
            info={PARTNERS.TENTH}
            src="/10mtn.webp"
            href={LINKS.TEN_MTN}
          />
          <SponsorContainer
            title="Tier-1 Pro Inspections"
            info={PARTNERS.TIER}
            src="/tier1.webp"
            href={LINKS.TIER1}
            showLightBg
          />
          <SponsorContainer
            title="Project Roll Call"
            info={PARTNERS.PROJECT_ROLL_CALL}
            src="/ProjectRollCall.webp"
            href={LINKS.PROJECT_ROLL_CALL}
            width={200}
            height={100}
          />
          <SponsorContainer
            title="Help Me PTSD"
            info={PARTNERS.HELP_ME_PTSD}
            src="/HelpPtsd.webp"
            href={LINKS.HELP_ME_PTSD}
            showLightBg
          />
          <SponsorContainer
            title="AZ Metro News"
            info={PARTNERS.AZ_METRO_NEWS}
            src="/Azmetro.webp"
            href={LINKS.AZ_METRO}
            showLightBg
          />
        </Box>
      </Box>
    </>
  );
};

Partners.propTypes = {};

export default Partners;

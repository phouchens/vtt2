import MeetTheTeam from '@components/aboutPageLayout/MeetTheTeam/MeetTheTeam';
import Mission from '@components/aboutPageLayout/Mission/Mission';
import ConnectBar from '@components/connectBar/ConnectBar';
import AboutHeaderContainer from '@components/halfScreenContainers/AboutHeaderContainer';
import Community from '@components/shared/Community';
import ListenNow from '@components/shared/ListenNow';
import { Box } from 'grommet';
import Head from 'next/head';

export default function About() {
  return (
    <>
      <Head>
        <title>About us: Veteran Trash Talk</title>
      </Head>
      <Box fill="horizontal">
        <AboutHeaderContainer />
        <Box pad="small">
          <Mission />
          <MeetTheTeam />
          <ConnectBar />
          <Community />
          <ListenNow />
        </Box>
      </Box>
    </>
  );
}

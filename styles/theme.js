export const vttGray3 = '#333';
export const vttBlack = '#0d0d0d';
export const vttGreen = '#394004';
export const vttOrange = '#F5A33A';
export const vttWhite = '#FFFFFF';

export const radius = '5px';
const theme = {
  rounding: 6,
  spacing: 24,
  defaultMode: 'dark',
  global: {
    colors: {
      brand: {
        dark: vttOrange,
        light: vttOrange
      },
      background: {
        dark: vttBlack,
        light: vttWhite
      },
      'background-back': {
        dark: vttBlack,
        light: '#EEEEEE'
      },
      'background-front': {
        dark: vttBlack,
        light: vttWhite
      },
      'background-contrast': {
        dark: vttBlack,
        light: '#121212'
      },
      text: {
        dark: '#EEEEEE',
        light: '#333333'
      },
      'text-strong': {
        dark: vttWhite,
        light: '#000000'
      },
      'text-weak': {
        dark: '#CCCCCC',
        light: '#444444'
      },
      'text-xweak': {
        dark: '#999999',
        light: '#666666'
      },
      border: {
        dark: '#444444',
        light: '#CCCCCC'
      },
      control: 'brand',
      'active-background': 'background-contrast',
      'active-text': 'text-strong',
      'selected-background': 'brand',
      'selected-text': 'text-strong',
      'status-critical': '#FF4040',
      'status-warning': '#FFAA15',
      'status-ok': '#00C781',
      'status-unknown': '#CCCCCC',
      'status-disabled': '#CCCCCC',
      'graph-0': 'brand',
      'graph-1': 'status-warning',
      focus: {
        light: '#faa916',
        dark: '#faa916'
      }
    },
    font: {
      family: 'Roboto',
      size: '14px',
      height: '20px',
      face:
        "@font-face {\n  font-family: 'Roboto';\n  font-style: normal;\n  font-weight: 400;\n font-display: fallback;\n  src: url(https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');\n  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;\n}\n\n/* latin */\n@font-face {\n  font-family: 'Open Sans';\n  font-style: normal;\n font-display: fallback;\n  font-weight: 400;\n  src: url(https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFVZ0b.woff2) format('woff2');\n}\n"
    },
    active: {
      background: 'active-background',
      color: 'active-text'
    },
    hover: {
      background: 'active-background',
      color: 'active-text'
    },
    selected: {
      background: 'selected-background',
      color: 'selected-text'
    },
    control: {
      border: {
        radius: radius
      }
    },
    drop: {
      border: {
        radius: radius
      }
    }
  },
  chart: {},
  diagram: {
    line: {}
  },
  meter: {},
  button: {
    primary: {
      extend: `color:${vttBlack};`
    },
    border: {
      radius: radius
    }
  },
  checkBox: {
    color: '#8C8C8C',
    size: '18px',
    check: {
      radius: radius
    },
    toggle: {
      radius: radius
    }
  },
  radioButton: {
    check: {
      radius: radius
    }
  },
  formField: {
    border: {
      color: vttWhite,
      error: {
        margin: 'none',
        size: 'xsmall',
        color: {
          dark: 'white',
          light: 'status-critical'
        }
      },
      position: 'inner',
      side: 'all'
    },
    content: {
      pad: 'small'
    },
    disabled: {
      background: {
        color: 'status-disabled',
        opacity: 'medium'
      }
    },
    error: {
      color: 'status-critical',
      size: 'xsmall',
      margin: {
        vertical: 'none',
        horizontal: 'small'
      }
    },
    help: {
      color: 'dark-3',
      margin: {
        start: 'small'
      }
    },
    info: {
      color: 'text-xweak',
      margin: {
        vertical: 'xsmall',
        horizontal: 'small'
      }
    },
    label: {
      color: vttWhite,
      margin: {
        vertical: 'none',
        horizontal: 'small'
      }
    },
    margin: {
      bottom: 'small'
    },
    round: radius
  },
  heading: {
    font: {
      family: 'Open Sans'
    }
  }
};

export default theme;

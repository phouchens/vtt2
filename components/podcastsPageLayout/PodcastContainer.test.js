import React from 'react';
import { shallow } from 'enzyme';
import PodcastContainer from './PodcastContainer';

describe('PodcastContainer tests', () => {
  const props = {
    src: '/test',
    title: 'test',
    about: 'tester',
    href: 'test.com'
  };
  it('should render without crashing', () => {
    const wrapper = shallow(<PodcastContainer {...props} />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

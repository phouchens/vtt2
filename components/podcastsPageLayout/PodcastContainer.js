import React from 'react';
import PropTypes from 'prop-types';
import { Box, Heading, Text } from 'grommet';
import LinkButton from '../shared/LinkButton';
import Image from 'next/image';

const PodcastContainer = ({ src, title, about, href }) => {
  return (
    <Box direction="column" align="center" pad="medium">
      <Box margin="xsmall">
        <Image src={src} width={350} height={200} />
      </Box>
      <Heading level="2" textAlign="center" margin={{ top: '1em' }}>
        {title}
      </Heading>
      <Text textAlign="center" size="small" margin={{ bottom: '2em' }}>
        {about}
      </Text>
      <LinkButton href={href} label="Listen Now" />
    </Box>
  );
};

PodcastContainer.propTypes = {
  src: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  about: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired
};

export default PodcastContainer;

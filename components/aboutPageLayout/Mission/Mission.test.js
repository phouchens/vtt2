import React from 'react';
import { shallow } from 'enzyme';
import Mission from './Mission';

describe('Mission tests', () => {
  it('should render', () => {
    const wrapper = shallow(<Mission />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

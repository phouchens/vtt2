import React from 'react';
import { Box, Heading } from 'grommet';
import TeamMember from './TeamMember';

const JOE_ABOUT =
  'Joe is the behind the scenes Swiss army knife that can get it done and doesn’t mind getting his hands dirty. He definitely disputes that Dave is the best looking one in the group. He excels at any sport involving a ball or anything that fires a projectile.';
const DAVE_ABOUT =
  'Dave is the “color guy”, trilingual, and best looking of the group. If he had 6 abs and gave zero f*cks, how many vodkas would it take him to tell you about his early 90s rap career? Null.';

const NICK_ABOUT =
  'Nick is a self proclaimed comedic genius and was voted class clown in 1999. His actual bio is too big to fit on the page.';

const MATT_ABOUT =
  'Matt is the only “civvy” in the group and is a bona fide business geek.  Fortunately for the math-challenged founders, he has degrees in economics and finance.  In addition to managing the books for VTT, Matt works as a private wealth advisor in Chicago, IL.';

const MeetTheTeam = () => {
  return (
    <Box direction="column" justify="center" align="center">
      <Heading level="2">Meet the Team - VTT Actual</Heading>
      <TeamMember
        src="/JoeB.webp"
        fallback="/JoeB.jpg"
        imageSize="large"
        name="Joe Bridson"
        aboutText={JOE_ABOUT}
      />
      <TeamMember
        src="/DavidT.webp"
        fallback="/DavidT.jpg"
        imageSize="large"
        name="David Trentin"
        aboutText={DAVE_ABOUT}
      />
      <TeamMember
        src="/NickC.webp"
        fallback="/NickC.jpg"
        imageSize="large"
        name="Nick Cotrill"
        aboutText={NICK_ABOUT}
      />
      <TeamMember
        src="/Matt.webp"
        fallback="/Matt.jpeg"
        imageSize="large"
        name="Matthew J. Grezlik"
        aboutText={MATT_ABOUT}
      />
    </Box>
  );
};

export default MeetTheTeam;

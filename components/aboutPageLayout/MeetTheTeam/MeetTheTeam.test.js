import React from 'react';
import { shallow } from 'enzyme';
import MeetTheTeam from './MeetTheTeam';

describe('MeetTheTeam tests', () => {
  it('should render', () => {
    const wrapper = shallow(<MeetTheTeam />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

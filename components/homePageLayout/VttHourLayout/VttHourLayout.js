import React from 'react';
import { Box, Heading, Paragraph } from 'grommet';
import Image from 'next/image';

const VttHourLayout = () => {
  return (
    <Box
      direction="row"
      wrap
      margin={{ top: '2em', bottom: '2em' }}
      justify="center"
      align="center">
      <Box width="medium" justify="center">
        <Image src="/soldier-and-dog.webp" width={397} height={258} />
      </Box>
      <Box
        direction="column"
        pad="small"
        margin={{ bottom: '1em' }}
        aligne="center"
        justify="center">
        <Heading level="3" textAlign="center" margin="none">
          Veteran Trash Talk Hour Podcast
        </Heading>
        <Paragraph textAlign="center" margin={{ bottom: '1em' }} alignSelf="center">
          Veteran Trash Talk Hour podcasts are stories of real warriors dealing with demons on the
          inside. We are brave enough to share them with our brothers, sisters, supporters, and
          contributors so we can defeat them together. Our podcasts promote veteran-owned businesses
          and organizations that support veteran causes. Also, there is plenty of trash talk.
        </Paragraph>
      </Box>
    </Box>
  );
};

export default VttHourLayout;

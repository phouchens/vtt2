import React from 'react';
import { shallow } from 'enzyme';
import MeetTheHosts from './MeetTheHosts';

describe('MeetTheHosts tests', () => {
  it('should render', () => {
    const wrapper = shallow(<MeetTheHosts />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

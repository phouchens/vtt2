import React from 'react';
import { shallow } from 'enzyme';
import WhatPeopleAreSaying from './WhatPeopleAreSaying';

describe('WhatPeopleAreSaying tests', () => {
  it('should render', () => {
    const wrapper = shallow(<WhatPeopleAreSaying />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

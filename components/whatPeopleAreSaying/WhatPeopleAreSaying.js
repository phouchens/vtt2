import React from 'react';
import { Box, Heading, ResponsiveContext } from 'grommet';
import Quote from './Quote';

const JOEL_QUOTE =
  'Just wanted to say how amazing I think the VTT group is. Funny, sarcastic, trash talk to call people out, but always there for each other... just like the real military.';

const RYAN_QUOTE =
  'Fantastic work. I know some of these guys personally from our time vacationing in the Middle East. A+ content.';

const JEFF_QUOTE =
  "I've been seriously injured. It has changed my life. The support I get from VTT and it's members keep me driving and pushing";

const JUSTIN_QUOTE = "It's not the worst veteran group on Facebook.";

const DONOVAN_QUOTE = 'Greatest bunch of Tom, Dicks and Harrys on Facebook';

const WhatPeopleAreSaying = () => {
  const size = React.useContext(ResponsiveContext);

  return (
    <Box direction="column" justify="center" align="center" margin="small">
      <Heading level="2">What People are Saying</Heading>
      <Box direction={size === 'small' ? 'column' : 'row'}>
        <Quote name="Joel" text={JOEL_QUOTE} />
        <Quote name="Ryan" text={RYAN_QUOTE} />
        <Quote name="Jeff" text={JEFF_QUOTE} />
      </Box>
      <Box direction={size === 'small' ? 'column' : 'row'}>
        <Quote name="Justin" text={JUSTIN_QUOTE} />
        <Quote name="Donovan" text={DONOVAN_QUOTE} />
      </Box>
    </Box>
  );
};

export default WhatPeopleAreSaying;

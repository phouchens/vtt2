import React from 'react';
import PropTypes from 'prop-types';
import { Box, Paragraph, Text } from 'grommet';

const Quote = ({ name, text }) => {
  return (
    <Box width="medium" margin="small">
      <Paragraph textAlign="center">
        <q>{text}</q>
      </Paragraph>
      <Text textAlign="center">-{name}</Text>
    </Box>
  );
};

Quote.propTypes = {
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
};

export default Quote;

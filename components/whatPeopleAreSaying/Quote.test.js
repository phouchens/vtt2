import React from 'react';
import { shallow } from 'enzyme';
import Quote from './Quote';

describe('Quote tests', () => {
  const props = {
    name: 'jon',
    text: 'tes'
  };
  it('should render', () => {
    const wrapper = shallow(<Quote {...props} />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

import React from 'react';
import { Box, Heading, ResponsiveContext, Text } from 'grommet';
import BoxWithBGImage from '@components/shared/BoxWithBGImage';
import ContactForm from './ContactForm';
import { vttWhite } from '@styles/theme';

const ContactUs = ({ gradient = true }) => {
  const size = React.useContext(ResponsiveContext);

  return (
    <BoxWithBGImage
      url="/soldierWithGun.webp"
      gradient={gradient}
      height="medium"
      pad={{ bottom: '1em' }}
      align="center"
      justify="around"
      direction={size === 'small' ? 'column' : 'row'}>
      <Box
        id="verbiage"
        direction="column"
        justify="center"
        align="center"
        width={size !== 'small' ? { max: '50%' } : 'large'}
        pad="medium">
        <Heading level="3" textAlign="center" margin="none" color={vttWhite}>
          Get In Touch
        </Heading>
        <Text
          weight="bold"
          textAlign="center"
          size={size === 'small' ? 'xsmall' : 'small'}
          color={vttWhite}>
          Want to be on the show? Want us to give a shout out to a veteran owned business? Want to
          send us mail for us to read on the air? Hit us up!
        </Text>
      </Box>
      <ContactForm />
    </BoxWithBGImage>
  );
};

export default ContactUs;

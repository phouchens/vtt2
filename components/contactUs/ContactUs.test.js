import React from 'react';
import { shallow } from 'enzyme';
import ContactUs from './ContactUs';

describe('ContactUs tests', () => {
  it('should render', () => {
    const wrapper = shallow(<ContactUs />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

import React from 'react';
import { shallow } from 'enzyme';
import AvitarWithText from './AvatarWIthText';

describe('avatar with text', () => {
  it('should render', () => {
    const props = { size: 'small', src: '/test.jpg', fallback: '/test.png', name: 'test' };
    const wrapper = shallow(<AvitarWithText {...props} />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

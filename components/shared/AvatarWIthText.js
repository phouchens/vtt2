import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Box, Heading } from 'grommet';

const AvatarWIthText = ({ size, src, fallback, name }) => {
  return (
    <Box justify="center" align="center">
      <Avatar src={src} size={size} fallback={fallback} />
      <Heading level="4" margin=".5em">
        {name}
      </Heading>
    </Box>
  );
};

AvatarWIthText.propTypes = {
  size: PropTypes.string.isRequired,
  fallback: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default AvatarWIthText;

import React from 'react';
import { shallow } from 'enzyme';
import FormSubmitButton from './FormSubmitButton';

describe('FormSubmitButton tests', () => {
  it('should render', () => {
    const status = { submitting: false, submitted: false, info: { error: false, msg: null } };
    const wrapper = shallow(<FormSubmitButton status={status} />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });

  it('should render error message', () => {
    const status = { submitting: false, submitted: false, info: { error: true, msg: 'error' } };
    const wrapper = shallow(<FormSubmitButton status={status} />);
    expect(wrapper.find('Text').props().children).toContain(status.info.msg);
  });

  it('should render Spinner', () => {
    const status = { submitting: true, submitted: false, info: { error: false, msg: null } };
    const wrapper = shallow(<FormSubmitButton status={status} />);
    expect(wrapper.find('Button').props().label).toBeTruthy();
  });

  it('should render button label as submitted', () => {
    const status = { submitting: false, submitted: true, info: { error: false, msg: null } };
    const wrapper = shallow(<FormSubmitButton status={status} />);
    expect(wrapper.find('Button').props().label).toContain('Submitted');
  });
});

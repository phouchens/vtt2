import React from 'react';
import { shallow } from 'enzyme';
import LinkButton from './LinkButton';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '',
      query: '',
      asPath: ''
    };
  }
}));

describe('link button component', () => {
  it('should render', () => {
    const props = { href: '/', label: 'Push Me' };
    const wrapper = shallow(<LinkButton {...props} />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

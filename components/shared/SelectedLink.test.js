import React from 'react';
import { shallow } from 'enzyme';
import SelectedLink from './SelectedLink';
jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '/',
      query: '',
      asPath: ''
    };
  }
}));
describe('selected link  component', () => {
  it('should render', () => {
    const props = { name: 'home', href: '/', changeState: jest.fn() };
    const wrapper = shallow(<SelectedLink {...props} />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
    expect(wrapper.find({ 'data-testid': 'styledLink' }).props().selected).toEqual(true);
  });

  it('should not be selected', () => {
    const props = { name: 'home', href: '/', hideSelection: true, changeState: jest.fn() };
    const wrapper = shallow(<SelectedLink {...props} />);
    expect(wrapper.find({ 'data-testid': 'styledLink' }).props().selected).toEqual(false);
  });
});

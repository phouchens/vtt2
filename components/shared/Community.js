import React from 'react';
import { Box, Text } from 'grommet';
import { Facebook } from 'grommet-icons';
import ClickableCard from '../callToAction/ClickableCard';
import { LINKS } from 'helpers/globals';
import { vttOrange } from '../../styles/theme';

const Community = () => {
  return (
    <Box direction="column" align="center" justify="center" margin="medium">
      <Box justify="center" align="center" pad="medium">
        <Text size="xlarge" weight="bold">
          JOIN OUR COMMUNITY
        </Text>
      </Box>
      <Text textAlign="center">
        We have built a thriving community on Facebook. There you can find support, resources, and
        plenty of trash talk.
      </Text>
      <ClickableCard
        linkTo={LINKS.FACEBOOK_GROUP}
        icon={<Facebook size="xlarge" color={vttOrange} />}
        text="Join Our Facebook Group"
        level="4"
      />
    </Box>
  );
};

export default Community;

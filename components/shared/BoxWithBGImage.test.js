import React from 'react';
import { shallow } from 'enzyme';
import BoxWithBGImage from './BoxWithBGImage';

describe('BoxWithBGImage tests', () => {
  it('should render', () => {
    const props = {
      gradient: true,
      url: 'test'
    };
    const wrapper = shallow(<BoxWithBGImage {...props} />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

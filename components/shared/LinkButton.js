import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import styled from 'styled-components';
import { radius, vttBlack, vttOrange } from '@styles/theme';

const StyledLink = styled.a`
  display: inline-block;
  box-sizing: border-box;
  cursor: pointer;
  font: inherit;
  text-decoration: none;
  margin: 0;
  align-self: center;
  border: 2px solid ${vttOrange};
  border-radius: ${radius};
  padding: 4px 22px;
  line-height: 24px;
  background-color: ${vttOrange};
  color: ${vttBlack};
`;

const LinkButton = ({ href, label }) => {
  return (
    <Link href={href} passHref>
      <StyledLink target="_blank"> {label}</StyledLink>
    </Link>
  );
};

LinkButton.propTypes = {
  href: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
};

export default LinkButton;

import React from 'react';

const useFormHandler = (emptyInput, apiUrl, setFunction) => {
  const [status, setStatus] = React.useState({
    submitted: false,
    submitting: false,
    info: { error: false, msg: null }
  });

  const handleResponse = (resStatus, msg) => {
    if (resStatus === 200) {
      setStatus({
        submitted: true,
        submitting: false,
        info: { error: false, msg }
      });
      setFunction(emptyInput);
    } else {
      setStatus({
        submitted: false,
        submitting: false,
        info: { error: true, msg }
      });
    }
  };

  const handleOnSubmit = async (e, formValues) => {
    e.preventDefault();
    setStatus((prevStatus) => ({ ...prevStatus, submitting: true }));
    const res = await fetch(`/api/${apiUrl}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formValues)
    });
    const text = await res.text();
    handleResponse(res.status, text);
  };

  return {
    status,
    handleOnSubmit
  };
};

export default useFormHandler;

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Box } from 'grommet';
import { vttBlack } from '@styles/theme';

const StyledBox = styled(Box)`
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  background-image: ${(props) =>
    props.gradient
      ? `-webkit-linear-gradient( rgba(00, 00, 00, .22591034704897583) 10%, ${vttBlack} 100%), url(${props.url})`
      : `url(${props.url})`};
`;

const BoxWithBGImage = ({ ...rest }) => {
  return <StyledBox {...rest} />;
};

BoxWithBGImage.propTypes = {
  url: PropTypes.string.isRequired,
  gradient: PropTypes.bool
};

BoxWithBGImage.defaultProps = {
  gradient: false
};

export default BoxWithBGImage;

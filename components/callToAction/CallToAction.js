import React from 'react';
import { Box, ResponsiveContext } from 'grommet';
import { Cart, Spotify, Youtube } from 'grommet-icons';
import ClickableCard from './ClickableCard';
import { vttOrange } from '../../styles/theme';
import { LINKS } from '../../helpers/globals';

const CallToAction = () => {
  const size = React.useContext(ResponsiveContext);

  return (
    <Box data-testid="call-to-action" pad="small" direction="column">
      <Box direction={size == 'small' ? 'column' : 'row'} align="center">
        <ClickableCard
          linkTo={LINKS.SHOP}
          icon={<Cart size="xlarge" color={vttOrange} />}
          text="shop"
        />
        <ClickableCard
          linkTo={LINKS.SPOTIFY}
          icon={<Spotify size="xlarge" color={vttOrange} />}
          text="listen"
        />
        <ClickableCard
          linkTo={LINKS.YOUTUBE}
          text="watch"
          icon={<Youtube size="xlarge" color={vttOrange} />}
        />
      </Box>
    </Box>
  );
};

export default CallToAction;

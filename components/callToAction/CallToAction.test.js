import React from 'react';
import { shallow } from 'enzyme';
import CallToAction from './CallToAction';
jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '',
      query: '',
      asPath: ''
    };
  }
}));

describe('call to action  component', () => {
  it('should render', () => {
    const wrapper = shallow(<CallToAction />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

import React from 'react';
import { shallow } from 'enzyme';
import { Spotify } from 'grommet-icons';
import ClickableCard from './ClickableCard';
global.open = jest.fn();
describe('ClickableCard tests', () => {
  it('should render', () => {
    const props = { linkTo: 'test.com', icon: <Spotify />, text: 'test' };
    const wrapper = shallow(<ClickableCard {...props} />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });

  it('should call open new window on click', () => {
    const props = { linkTo: 'test.com', icon: <Spotify />, text: 'test' };

    const wrapper = shallow(<ClickableCard {...props} />);
    wrapper.find({ data_testid: 'card' }).simulate('click');
    expect(global.open).toHaveBeenCalled();
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

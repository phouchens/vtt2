import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import Image from 'next/image';
import ColoredBoxBanner from './ColoredBoxBanner';

const BannerContainer = ({ href, src, color, height = 90, width = 728 }) => {
  return (
    <ColoredBoxBanner justify="center" align="center" color={color}>
      <Link href={href} passHref>
        <a target="_blank">
          <Image src={src} height={height} width={width} />
        </a>
      </Link>
    </ColoredBoxBanner>
  );
};

BannerContainer.propTypes = {
  href: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  height: PropTypes.number,
  width: PropTypes.number
};

export default BannerContainer;

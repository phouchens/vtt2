import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Box } from 'grommet';

const ColoredBox = styled(Box)`
  background: ${(props) => `${props.color}`};
  background: linear-gradient(
    90deg,
    rgba(38, 69, 154, 1) 19%,
    rgba(35, 35, 98, 1) 40%,
    rgba(15, 21, 63, 1) 80%
  );
  div:first-of-type {
    display: block !important;
  }
`;

const ColoredBoxBanner = ({ ...rest }) => {
  return <ColoredBox {...rest} />;
};

ColoredBoxBanner.propTypes = {
  color: PropTypes.string.isRequired
};

export default ColoredBoxBanner;

import React from 'react';
import { shallow } from 'enzyme';
import BannerContainer from './BannerContainer';

describe('BannerContainer tests', () => {
  it('renders without crashing', () => {
    const props = {
      href: 'test.com',
      src: '/image',
      color: '#fff'
    };
    const wrapper = shallow(<BannerContainer {...props} />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

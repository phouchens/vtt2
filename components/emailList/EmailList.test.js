import React from 'react';
import { shallow } from 'enzyme';
import EmailList from './EmailList';

describe('EmailList tests', () => {
  it('should render', () => {
    const wrapper = shallow(<EmailList />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

import React from 'react';
import { Box, Form, FormField, TextInput } from 'grommet';
import useFormHandler from '@components/shared/UseFormHandler';
import FormSubmitButton from '@components/shared/FormSubmitButton';

const EmailListForm = () => {
  const emptyForm = { email: '', name: '' };
  const [emailListValue, setEmailListValue] = React.useState(emptyForm);
  const { status, handleOnSubmit } = useFormHandler(emptyForm, 'emailList', setEmailListValue);

  return (
    <Box width="medium" pad="medium">
      <Form
        value={emailListValue}
        validate="blur"
        onChange={(nextEmailListValue) => setEmailListValue(nextEmailListValue)}
        onSubmit={(e) => handleOnSubmit(e, emailListValue)}>
        <FormField name="name" htmlfor="list-name-input-id" label="name" required>
          <TextInput id="list-name-input-id" name="name" />
        </FormField>
        <FormField name="email" htmlfor="list-email-input-id" label="email" required>
          <TextInput id="list-email-input-id" name="email" type="email" />
        </FormField>
        <FormSubmitButton status={status} />
      </Form>
    </Box>
  );
};

EmailListForm.propTypes = {};

export default EmailListForm;

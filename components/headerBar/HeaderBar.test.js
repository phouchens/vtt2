import React from 'react';
import { shallow } from 'enzyme';
import HeaderBar from './HeaderBar';

describe('HeaderBar tests', () => {
  it('should render', () => {
    const wrapper = shallow(<HeaderBar />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });

  it('should render side menu', () => {
    const props = { size: 'small', darkMode: true };
    const wrapper = shallow(<HeaderBar {...props} />);
    wrapper.find('#main-menu-button').simulate('click');
    expect(wrapper.find('#side-menu')).toHaveLength(1);
  });

  it('should close side menu', () => {
    const props = { size: 'small', darkMode: true };
    const wrapper = shallow(<HeaderBar {...props} />);
    // open menu
    wrapper.find('#main-menu-button').simulate('click');

    // close menu
    wrapper.find('#close-menu-button').simulate('click');

    expect(wrapper.find('#side-menu')).toHaveLength(0);
  });
});

import React from 'react';
import { shallow } from 'enzyme';
import AppBar from './AppBar';

describe('AppBar tests', () => {
  it('should render', () => {
    const wrapper = shallow(<AppBar />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

import React from 'react';
import { shallow } from 'enzyme';
import PartnerHeaderContainer from './PartnerHeaderContainer';

describe('PartnerHeaderContainer tests', () => {
  it('should render', () => {
    const wrapper = shallow(<PartnerHeaderContainer />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

import React from 'react';
import BoxWithBGImage from '../../components/shared/BoxWithBGImage';
import { Heading, ResponsiveContext, Text } from 'grommet';
import { vttOrange } from '../../styles/theme';
import styled from 'styled-components';

const HeadingText = styled(Heading)`
  font-weight: 700;
  font-size: ${(props) => (props.size === 'small' ? `60px` : '8vw')};
  padding: 0px;
  text-align: center;
  letter-spacing: 1px;
  line-height: 1em;
`;

const AboutHeaderContainer = () => {
  const size = React.useContext(ResponsiveContext);

  return (
    <BoxWithBGImage
      url="/about.webp"
      gradient
      height="medium"
      pad={{ bottom: '1em' }}
      align="center"
      justify="start"
      direction="column">
      <HeadingText
        size={size}
        color={vttOrange}
        margin={size === 'small' ? { bottom: 'none' } : { bottom: '1em' }}>
        ABOUT VETERAN TRASH TALK
      </HeadingText>
      <Text size="large" textAlign="center">
        The real demon is inside and not out. Send it out so we can destroy it. Never be afraid to
        “Send It”.
      </Text>
    </BoxWithBGImage>
  );
};

export default AboutHeaderContainer;

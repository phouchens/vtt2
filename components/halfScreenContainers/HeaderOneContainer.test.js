import React from 'react';
import { shallow } from 'enzyme';
import HeaderOneContainer from './HeaderOneContainer';

describe('HeaderOneContainer tests', () => {
  it('should render', () => {
    const wrapper = shallow(<HeaderOneContainer />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

import React from 'react';
import { shallow } from 'enzyme';
import PodcastsHeaderContainer from './PodcastsHeaderContainer';

describe('PodcastsHeaderContainer tests', () => {
  it('should render without crashing', () => {
    const wrapper = shallow(<PodcastsHeaderContainer />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

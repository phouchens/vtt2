import React from 'react';
import PropTypes from 'prop-types';
import { Box, Heading, Text } from 'grommet';
import Image from 'next/image';
import LinkButton from '../shared/LinkButton';
import styled from 'styled-components';
import { vttBlack, vttWhite } from '@styles/theme';

const ContainerTitle = styled(Heading)`
  margin-bottom: 5px;
`;
const SponsorContainer = ({
  featured = false,
  title,
  info,
  src,
  href,
  width = 200,
  height = 200,
  showLightBg = false,
  lightBgPad = false
}) => {
  return (
    <Box direction="column" align="center" pad="medium" border="bottom">
      {featured && (
        <ContainerTitle level="3" textAlign="center" margin="none">
          {title}
        </ContainerTitle>
      )}
      <Box
        background={showLightBg ? vttWhite : vttBlack}
        margin="xsmall"
        pad={lightBgPad ? 'small' : 'none'}>
        <Image src={src} width={width} height={height} />
      </Box>
      {!featured && (
        <ContainerTitle level="3" textAlign="center" margin="none">
          {title}
        </ContainerTitle>
      )}
      <Text textAlign="center" size="small" margin={{ bottom: '2em', top: '1em' }}>
        {info}
      </Text>
      <LinkButton href={href} label="Learn More" />
    </Box>
  );
};

SponsorContainer.propTypes = {
  title: PropTypes.string.isRequired,
  info: PropTypes.node.isRequired,
  src: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
  height: PropTypes.number,
  width: PropTypes.number,
  featured: PropTypes.bool,
  showLightBg: PropTypes.bool,
  lightBgPad: PropTypes.bool
};

export default SponsorContainer;

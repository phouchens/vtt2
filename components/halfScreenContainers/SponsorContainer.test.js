import React from 'react';
import { shallow } from 'enzyme';
import SponsorContainer from './SponsorContainer';

describe('SponsorContainer tests', () => {
  it('should render', () => {
    const props = {
      href: 'test',
      src: 'image',
      title: 'the title',
      info: 'info'
    };
    const wrapper = shallow(<SponsorContainer {...props} />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

import React from 'react';
import { Heading, ResponsiveContext, Text } from 'grommet';
import styled from 'styled-components';
import { vttOrange } from '../../styles/theme';
import BoxWithBGImageborder from '../../components/shared/BoxWithBGImage';

const HeadingText = styled(Heading)`
  font-weight: 700;
  font-size: ${(props) => (props.size === 'small' ? `60px` : '7vw')};
  padding: 0px;
  text-align: center;
  letter-spacing: 1px;
  line-height: 1em;
`;

const HeaderOneContainer = () => {
  const size = React.useContext(ResponsiveContext);

  return (
    <BoxWithBGImageborder
      url="/seaPeeps.webp"
      gradient
      height="medium"
      pad={{ bottom: '1em' }}
      align="center"
      justify="start"
      direction="column">
      <HeadingText
        size={size}
        color={vttOrange}
        margin={size === 'small' ? { bottom: 'none' } : { bottom: '1em' }}>
        VETERAN TRASH TALK
      </HeadingText>
      <Text size="medium" textAlign="center" margin="large">
        Stories about Veterans by Veterans that explore how to win inner battles and become
        successful.
      </Text>
    </BoxWithBGImageborder>
  );
};

export default HeaderOneContainer;

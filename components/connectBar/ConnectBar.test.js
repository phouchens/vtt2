import React from 'react';
import { shallow } from 'enzyme';
import ConnectBar from './ConnectBar';
jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '',
      query: '',
      asPath: ''
    };
  }
}));

describe('connect bar component', () => {
  it('should render', () => {
    const wrapper = shallow(<ConnectBar />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

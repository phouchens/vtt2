import React from 'react';
import { Anchor, Box, Heading, Nav } from 'grommet';
import { Apple, Facebook, Instagram, Spotify, Twitter, Youtube } from 'grommet-icons';
import { LINKS } from '../../helpers/globals';

const ConnectBar = () => {
  return (
    <Box
      direction="column"
      justify="center"
      align="center"
      background={{ light: 'light-5', dark: 'dark-1' }}
      pad="none"
      margin={{ top: '1em', bottom: '1em' }}>
      <Heading level="3" margin={{ bottom: '0px', top: '.5em' }}>
        Stay Connected
      </Heading>
      <Nav
        direction="row"
        justify="center"
        background={{ light: 'light-5', dark: 'dark-1' }}
        pad="none">
        <Anchor
          color={{ light: 'dark-1', dark: 'light-1' }}
          href={LINKS.FACEBOOK_PAGE}
          icon={<Facebook />}
          hoverIndicator
        />
        <Anchor
          color={{ light: 'dark-1', dark: 'light-1' }}
          href={LINKS.INSTAGRAM}
          icon={<Instagram />}
          hoverIndicator
        />
        <Anchor
          color={{ light: 'dark-1', dark: 'light-1' }}
          href={LINKS.YOUTUBE}
          icon={<Youtube />}
          hoverIndicator
        />
        <Anchor
          color={{ light: 'dark-1', dark: 'light-1' }}
          href={LINKS.SPOTIFY}
          icon={<Spotify />}
          hoverIndicator
        />
        <Anchor
          color={{ light: 'dark-1', dark: 'light-1' }}
          href={LINKS.APPLE}
          icon={<Apple />}
          hoverIndicator
        />
        <Anchor
          color={{ light: 'dark-1', dark: 'light-1' }}
          href={LINKS.TWITTER}
          icon={<Twitter />}
          hoverIndicator
        />
      </Nav>
    </Box>
  );
};

export default ConnectBar;

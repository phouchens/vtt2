import React from 'react';
import { shallow } from 'enzyme';
import FooterBar from './FooterBar';

describe('FooterBar tests', () => {
  it('should render', () => {
    const wrapper = shallow(<FooterBar />);
    expect(wrapper.isEmptyRender()).toBeFalsy();
  });
});

import React from 'react';
import styled from 'styled-components';
import { vttOrange } from '../../styles/theme';
import { Anchor, Footer, Box, Text, ResponsiveContext } from 'grommet';

import { Facebook, Instagram, Twitter, Youtube } from 'grommet-icons';

import SelectedLink from '../shared/SelectedLink';
import Link from 'next/link';
import Image from 'next/image';
import { StyledHeading } from '../headerBar/HeaderBar';
import { ABOUT_US, LINKS } from '../../helpers/globals';

const FooterContainer = styled(Box)``;

const FooterLink = styled.a`
  text-decoration: none;
  color: ${vttOrange};
  font-size: 14px;
  &:hover {
    border-bottom: 1px solid;
    background: ${vttOrange};
  }
`;

const FooterBar = () => {
  const size = React.useContext(ResponsiveContext);

  return (
    <Footer
      flex
      background={{ light: 'light-2', dark: '#111111' }}
      direction={size === 'small' ? 'column' : 'row'}
      align="center"
      justify="around"
      margin={{ top: '1em' }}
      pad="small">
      <FooterContainer direction="column" align="center" iner size={size}>
        <Box alignSelf="center">
          <Image src="/sss.webp" height="130" width="216" />
        </Box>

        <StyledHeading align="center" level={4}>
          STOP SOLDIER SUICIDE
        </StyledHeading>
        <Text size="small">Need to Talk?</Text>
        <Text size="small">Call 844-889-5610</Text>
        <Link href={LINKS.SSS} passHref>
          <FooterLink>stopsoldiersuicide.org</FooterLink>
        </Link>
      </FooterContainer>
      <FooterContainer size={size} align="center" alignSelf={size === 'small' ? 'center' : 'start'}>
        <StyledHeading level={4}>FOLLOW US</StyledHeading>
        <Text size="small">Trash talk with us!</Text>
        <Text size="small">#VETERANTRASHTALK</Text>
        <Box direction="row" wrap>
          <Anchor
            color={{ light: 'dark-1', dark: 'light-1' }}
            href={LINKS.FACEBOOK_PAGE}
            icon={<Facebook size="medium" />}
            hoverIndicator
          />
          <Anchor
            color={{ light: 'dark-1', dark: 'light-1' }}
            margin="none"
            href={LINKS.INSTAGRAM}
            icon={<Instagram size="medium" />}
            hoverIndicator
          />
          <Anchor
            color={{ light: 'dark-1', dark: 'light-1' }}
            href={LINKS.YOUTUBE}
            icon={<Youtube size="medium" />}
            hoverIndicator
          />
          <Anchor
            color={{ light: 'dark-1', dark: 'light-1' }}
            href={LINKS.TWITTER}
            icon={<Twitter size="medium" />}
            hoverIndicator
          />
        </Box>
      </FooterContainer>
      <FooterContainer size={size} direction="column" align="center">
        <Box direction="row" alignSelf="center">
          <Box flex={{ grow: 1 }}>
            <Image src="/logo.webp" height="163" width="104" />
          </Box>
          <Text color="light-1" alignSelf="end">
            &reg;
          </Text>
        </Box>

        <StyledHeading alignContent="center" level="4">
          VETERAN TRASH TALK LLC.
        </StyledHeading>
        <Text size="small">Contact us:</Text>
        <Text size="small" color={vttOrange}>
          Nick: {ABOUT_US.NICK_EMAIL}
        </Text>
        <Text size="small" color={vttOrange}>
          Joe: {ABOUT_US.JOE_EMAIL}
        </Text>
        <Text size="small" color={vttOrange}>
          Dave: {ABOUT_US.DAVE_EMAIL}
        </Text>
        <Box direction="row">
          <SelectedLink fontSize={12} href="/" name="home" hideSelection />
          <SelectedLink href="/about" name="about" hideSelection />
          <SelectedLink href="/partners" name="partners" hideSelection />
          <SelectedLink href={LINKS.SHOP} name="shop" hideSelection />
        </Box>
      </FooterContainer>
    </Footer>
  );
};

export default FooterBar;
